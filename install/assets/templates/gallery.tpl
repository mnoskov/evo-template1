/**
 * gallery
 *
 * Галерея
 *
 * @category       template
 */
{{head}}
{{page_head}}

<div class="container">
	[[sgLister?
		&parents=`[*id*]`
		&tpl=`gallery_images_tpl`
		&ownerTPL=`gallery_images_wrap`
		&prepare=`prepareGalleryItem`
		&prepareWrap=`prepareGalleryWrap`
	]]
	
	<div class="user-content">
		[*content*]
	</div>
</div>

{{footer}}