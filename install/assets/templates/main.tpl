/**
 * main
 *
 * Главная страница
 *
 * @category	   template
 */
{{head}}

[[PageBuilder? &container=`main_cycle`]]

[[PageBuilder? &container=`main`]]

{{footer}}