/**
 * service
 *
 * Услуга
 *
 * @category	   template
 */
{{head}}
{{page_head}}

<div class="container">
	<div class="row">
		<div class="col-md-3 col-sm-3 hidden-xs-down">
			[[DLMenu? 
				&parents=`[*parent*]` 
				&maxDepth=`1` 
				&outerTpl=`side_menu_wrap`
				&rowTpl=`side_menu_item`
			]]
		</div>
		
		<div class="col-md-9 col-sm-9">
			<div class="user-content">
				[*content*]
			</div>
		</div>
	</div>
</div>

{{footer}}