/**
 * info
 *
 * Страница по умолчанию
 *
 * @category       template
 */
{{head}}
{{page_head}}

<div class="container">
	<div class="user-content">
		[*content*]
	</div>
</div>

{{footer}}