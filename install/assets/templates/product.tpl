/**
 * product
 *
 * Товар
 *
 * @category	   template
 */
{{head}}

<div class="page-head gray">
	<div class="container">
		{{breadcrumbs}}

		<h1 class="page-title">
			[*pagetitle*]
		</h1>
		
		<div class="product-card">
			<div class="row">
				<div class="col-md-7">
					<div class="images-cycle">
						[[multiTV? &tvName=`gallery` &display=`all`]]
						[[multiTV? &tvName=`gallery` &display=`all` &rowTpl=`product_images_pager_item` &outerTpl=`product_images_pager_wrap`]]
					</div>
				</div>
				
				<div class="col-md-5">
					<div class="info">
						<div class="area themed-background">[*area*] м&sup2;</div>

						<div class="price[[if? &is=`[*old_price*]:!empty` &then=` with-old-price`]]">
							<div class="value">[[formatPrice? &in=`[*price*]`]]</div>
							[[if? &is=`[*old_price*]:!empty` &then=`<div class="old-price">Без скидки: <span class="value">[[formatPrice? &in=`[*old_price*]`]]</span></div>`]]
						</div>

						<div class="user-content">
							[*introtext*]
						</div>

						<div class="buttons">
							<a href="#" class="btn btn-theme" data-toggle="modal" data-target="#order" data-set-product="[*pagetitle*]">Заказать</a>
							<a href="#" class="btn btn-hollow-theme" data-toggle="modal" data-target="#question" data-set-product="[*pagetitle*]">Задать вопрос</a>
						</div>
						
						[[share]]
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="descriptions">
		[[if? &is=`[*content*]~!empty` &separator=`~` &then=`
			<div class="block-title">Описание</div>
			<div class="user-content">
				[*content*]
			</div>
		`]]
		
		[[multiTV? &tvName=`prints` &display=`all`]]
		
		[[renderAttributes? &category=`7` &tpl=`product_attributes_row` &outerTpl=`product_attributes_wrap` &exclude=`category,price`]]
		
		<div class="parent-link">
			<a href="[~[*parent*]~]"><i class="icon-left-arrow themed-background"></i>Вернуться в каталог</a>
		</div>
	</div>
</div>

{{footer}}