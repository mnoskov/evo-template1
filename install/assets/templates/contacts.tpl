/**
 * contacts
 *
 * Контакты
 *
 * @category       template
 */
{{head}}
{{page_head}}

<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="user-content">
                [*content*]
            </div>
        </div>

        <div class="col-md-7">
            [*map*]
        </div>
    </div>
</div>

{{footer}}
