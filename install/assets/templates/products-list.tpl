/**
 * products-list
 *
 * Список товаров
 *
 * @category	   template
 */
{{head}}
{{page_head}}

<div class="container">
	[[DocLister? 
		&parents=`[*id*]` 
		&tvList=`image`
		&tpl=`categories_tpl1_item` 
		&ownerTPL=`categories_tpl1_wrap` 
		&orderBy=`c.menuindex ASC`
	]]
	
	<div class="user-content">
		[*content*]
	</div>
</div>

{{footer}}