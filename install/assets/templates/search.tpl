/**
 * search
 *
 * Результаты поиска
 *
 * @category	   template
 */
{{head}}
{{page_head}}

[!eFilter? &cfg=`template1_search` &docid=`11` &depth=`4` &tvConfig=`[*tovarparams*]`!]

<div class="container">
	<div class="row">
		<div class="col-md-4 col-lg-3">
			[+eFilter_form+]
		</div>
		
		<div class="col-md-8 col-lg-9">
			[!eFilterResult? 
				&depth=`4` 
				&tvList=`image,area,price,old_price`
				&orderBy=`menuindex ASC`
				&tpl=`products_item` 
				&ownerTPL=`products_wrap` 
				&noneTPL=`@CODE:Ничего не найдено`
				&display=`12`
				&paginate=`pages`
				&TplCurrentPage=`@CODE: <span class="page current">[+num+]</span>` 
				&TplWrapPaginate=`@CODE:<div class="pagination">[+wrap+]</div>`
				&TplPrevP=`@CODE: `
				&TplNextP=`@CODE: `
			!]
			
			<div class="user-content">
				[*content*]
			</div>
		</div>
	</div>
</div>

{{footer}}