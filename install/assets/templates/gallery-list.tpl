/**
 * gallery-list
 *
 * Список галерей
 *
 * @category	   template
 */
{{head}}
{{page_head}}

<div class="container">
	[[DocLister? 
		&parents=`[*id*]`
		&tvList=`image`
		&tpl=`list_item` 
		&ownerTPL=`list_wrap` 
		&orderBy=`menuindex ASC` 
		&display=`10`
		&paginate=`pages`
		&TplCurrentPage=`@CODE: <span class="page current">[+num+]</span>` 
		&TplWrapPaginate=`@CODE:<div class="pagination">[+wrap+]</div>`
		&TplPrevP=`@CODE: `
		&TplNextP=`@CODE: `
	]]
</div>

{{footer}}