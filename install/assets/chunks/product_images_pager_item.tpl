/**
 * product_images_pager_item
 * 
 * product_images_pager_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="slide"><div style="background-image: url('[[thumb? &input=`[+image+]` &options=`w=127,h=80,f=jpg`]]');"></div></div>