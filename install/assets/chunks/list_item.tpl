/**
 * list_item
 * 
 * list_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="item">
    <div class="row">
        [[if? &is=`[+tv.image+]:!empty` &then=`
            <div class="col-sm-4 col-md-3 hidden-xs-down">
                <a href="[+url+]"><img src="[[thumb? &input=`[+tv.image+]` &options=`w=280,h=180,f=jpg`]]" alt="[+pagetitle+]" class="img-fluid"></a>
            </div>
            <div class="col-sm-8 col-md-9">
        ` &else=`
            <div class="col-sm-12">
        `]]
            <div class="title"><a href="[+url+]">[+pagetitle+]</a></div>
            <div class="intro">[+introtext+]</div>
            <a href="[+url+]" class="btn btn-hollow-theme">Подробнее</a>
        </div>
    </div>
</div>