/**
 * products_wrap
 * 
 * products_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="products-list">
    <div class="items">
        [+dl.wrap+]
        [+pages+]
    </div>
</div>