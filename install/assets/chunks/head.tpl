/**
 * head
 * 
 * head
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
{{header}}
    <div class="wrap">
        <div class="head-placeholder">
            <div class="head">
                <div class="container">
                    <div class="blocks-container">
                        <div class="left-block">
                            <div class="blocks-container">
                                <div class="logo">
                                    <a href="/">
                                        [[if? &is=`[(client_company_logo)]:!empty` &then=`
                                            <img src="[(client_company_logo)]" alt="[(site_name)]">
                                        ` &else=`
                                            {{logo}}
                                        `]]
                                    </a>
                                </div>

                                <a href="#" class="show-menu show-in-fixed">
                                    <i class="icon-menu"></i>
                                    <i class="icon-small-down-arrow"></i>
                                    Меню
                                </a>

                                <div class="show-on-top hidden-md-down text">
                                    [(client_head_text:addbreak)]
                                </div>
                            </div>
                        </div>

                        <div class="right-block text-xs-right">
                            <div class="wi-group phone">
                                <i class="icon-phone"></i>
                                [[splitRows? &in=`[(client_company_phone)]` &tpl=`phones_row`]]
                            </div>
                            <br>
                            <a href="#" data-toggle="modal" data-target="#callback" class="btn btn-hollow-theme callback">Заказать звонок</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="head-menu">
                <div class="container">
                    <a class="hidden-lg-up icon-menu show-menu" href="#"></a>
                    <div class="menu">
                        [[DLMenu? 
                            &parents=`1` 
                            &maxDepth=`2` 
                            &outerTpl=`head_menu_wrap`
                            &parentRowTpl=`head_menu_parent_item`
                            &rowTpl=`head_menu_item`
                            &addWhereList=`c.hidemenu != 1`
                        ]]
                    </div>
                </div>
            </div>
        </div>
