/**
 * categories_tpl2_item
 * 
 * categories_tpl2_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="col-md-4 col-sm-6">
    <div class="item">
        <div class="image">
            <a href="[+url+]">
                <img src="[[phpthumb? &input=`[+tv.image+]` &options=`w=100,h=100,zc=1,f=jpg`]]" class="img-fluid">
            </a>
        </div>
        
        <div class="info">
            <div class="title">
                <a href="[+url+]">[+pagetitle+]</a>
            </div>
            
            <div class="intro">
                [[nl2br? &in=`[+introtext+]`]]
            </div>
        </div>
    </div>
</div>
