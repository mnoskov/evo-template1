/**
 * categories_tpl1_item
 * 
 * categories_tpl1_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="col-md-4 col-sm-6">
    <div class="item">
        <div class="image" style="background-image: url('[[thumb? &input=`[+tv.image+]` &options=`w=365,h=270,f=jpg`]]');"></div>
        <div class="info">
            <div class="title"><span class="inline-padding"><span><span>[+pagetitle+]</span></span></span></div>
            <div class="intro">[[nl2br? &in=`[+introtext+]`]]</div>
        </div>
        <a href="[+url+]"></a>
    </div>
</div>