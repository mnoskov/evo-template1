/**
 * form_product
 * 
 * form_product
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<form method="post" action="#" class="ajax" data-goal="form:zakaz">
    <div class="form-content">
        <div class="form-title themed-background">Заказать расчет стоимости</div>
        <div class="form-group wi">
            <input type="text" name="name" class="form-control" placeholder="Ваше имя *">
            <i class="icon-user"></i>
        </div>

        <div class="form-group wi">
            <input type="text" name="phone" class="mask-phone form-control" placeholder="Контактый телефон *">
            <i class="icon-phone"></i>
        </div>

        <div class="text-xs-right">
            <input type="hidden" name="product" value="[*pagetitle*]">
            <input type="hidden" name="formid" value="product">
            <button type="submit" class="btn btn-theme">Отправить</button>
            
            {{policy_note}}
        </div>
    </div>
</form>