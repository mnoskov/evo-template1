/**
 * header
 *
 * header
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<!DOCTYPE html>
<html>
<head>
    <title>[[metatitle]]</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="/">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    [[metaheaders]]
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="assets/templates/default/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/templates/default/css/template.css?[!customizerStamp!]">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    [!IgnoreLighthouse? &content=`[(client_head_scripts)]`!]
</head>
<body class="[[template]]">
    [!IgnoreLighthouse? &content=`[(client_body_start_scripts)]`!]
