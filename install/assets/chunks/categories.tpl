/**
 * categories
 * 
 * categories
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
[[DocLister? 
    &parents=`4` 
    &tvList=`image`
    &tpl=`categories_tpl1_item` 
    &ownerTPL=`categories_tpl1_wrap` 
    &orderBy=`c.menuindex ASC`
]]