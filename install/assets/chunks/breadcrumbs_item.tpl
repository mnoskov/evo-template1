/**
 * breadcrumbs_item
 * 
 * breadcrumbs_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<a href="[+url+]" title="[+e.title+]">[+title+]</a><span class="separator">&mdash;</span>