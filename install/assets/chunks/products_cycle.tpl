/**
 * products_cycle
 * 
 * products_cycle
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="products-cycle">
    <div class="products-list">
        <div class="slick items" data-slick='{"slidesToShow": 4, "swipeToSlide": true, "responsive": [{"breakpoint": 992, "settings": {"slidesToShow": 3}}, {"breakpoint": 768, "settings": {"slidesToShow": 2}}, {"breakpoint": 576, "settings": "unslick"}]}'>
            [+dl.wrap+]
        </div>
    </div>
</div>