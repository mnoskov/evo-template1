/**
 * gallery_images_wrap
 * 
 * gallery_images_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="gallery format[+format+]">
    <ul class="items">
        [+dl.wrap+]
    </ul>
</div>