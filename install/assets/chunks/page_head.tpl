/**
 * page_head
 * 
 * page_head
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="page-head gray">
    <div class="container">
        {{breadcrumbs}}
        <h1 class="page-title">[[if? &is=`[*meta-title*]:!empty` &then=`[*meta-title*]` &else=`[[if? &is=`[*longtitle*]:!empty` &then=`[*longtitle*]` &else=`[*pagetitle*]`]]`]]</h1>
    </div>
</div>