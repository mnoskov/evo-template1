/**
 * head_menu_parent_item
 * 
 * head_menu_parent_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<li class="category-menu-item [+classnames+]"><a href="[+url+]" title="[+e.title+]"><i class="icon-small-down-arrow"></i><span>[+title+]</span></a>[+wrap+]</li>