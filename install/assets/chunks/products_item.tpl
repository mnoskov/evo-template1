/**
 * products_item
 * 
 * products_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="slide">
    <div class="item">
        <a href="[+url+]" class="image">
            <span style="background-image: url('[[thumb? &input=`[+tv.image+]` &options=`w=256,h=192,f=jpg`]]');"></span>
        </a>

        [[renderProductLabels? &docid=`[+id+]`]]

        <div class="info">
            <div class="title">
                <a href="[+url+]">[+pagetitle+]</a>
            </div>

            <div class="price">
                [[formatPrice? &in=`[+tv.price+]`]]
            </div>
        </div>
    </div>
</div>