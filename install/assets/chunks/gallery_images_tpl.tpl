/**
 * gallery_images_tpl
 * 
 * gallery_images_tpl
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<li>
    <a href="[+sg_image+]" data-fancybox="gallery" data-caption="[+e.sg_description+]">
        <span class="image" style="background-image: url('[[phpthumb? &input=`[+sg_image+]` &options=`[+dimensions+]`]]');"></span>
        
        <span class="text">
            [+sg_description+]
        </span>
    </a>