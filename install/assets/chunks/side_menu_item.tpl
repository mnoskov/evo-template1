/**
 * side_menu_item
 * 
 * side_menu_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<li[+classes+]><a href="[+url+]" title="[+e.title+]"><i class="icon-small-right-arrow"></i>[+title+]</a></li>