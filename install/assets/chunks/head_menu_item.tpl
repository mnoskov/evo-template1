/**
 * head_menu_item
 * 
 * head_menu_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<li[+classes+]><a href="[+url+]" title="[+e.title+]"><span>[+title+]</span></a></li>