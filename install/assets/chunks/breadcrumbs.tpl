/**
 * breadcrumbs
 * 
 * breadcrumbs
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
[[DLcrumbs?
    &showCurrent=`1`
    &addWhereList=`c.id != 1`
    &tpl=`breadcrumbs_item`
    &tplCurrent=`breadcrumbs_current`
    &ownerTPL=`breadcrumbs_wrap`
]]