/**
 * product_attributes_wrap
 * 
 * product_attributes_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<hr>

<div class="block-title">Характеристики</div>

<div class="row">
    <div class="col-md-6 col-lg-7 col-xl-8">
        <div class="attributes">
            <table>
                <tbody>
                    [+wrap+]
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-6 col-lg-5 col-xl-4">
        {{form_product}}
    </div>
</div>
