/**
 * categoryUpdate
 *
 * При сохранении и перемещении товаров обновляет их категорию, для использоваия в фильтрах
 *
 * @category    plugins
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal    @events onAfterMoveDocument,OnDocFormSave
 */
$e = $modx->Event;

$tablesc  = $modx->getFullTableName('site_content');
$tabletvc = $modx->getFullTableName('site_tmplvar_contentvalues');

switch ($e->name) {
    case 'onAfterMoveDocument': {
        $docid    = $e->params['id_document'];
        $parent   = $e->params['new_parent'];
        $template = $modx->db->getValue($modx->db->select('*', $tablesc, "id = '$docid'"));
        break;
    }
        
    case 'OnDocFormSave': {
        $docid = $e->params['id'];
        $query = $modx->db->select('parent, template', $tablesc, "id = '$docid'");
        list($parent, $template) = $modx->db->getRow($query, 'num');
        break;
    }
}

if (!empty($docid) && !empty($parent)) {
    $template_id = $modx->db->getValue($modx->db->select('id', $modx->getFullTableName('site_templates'), "templatename = 'product'"));

    if ($template == $template_id) {
        $tv_id = $modx->db->getValue($modx->db->select('id', $modx->getFullTableName('site_tmplvars'), "name = 'category'"));
        $query = $modx->db->select('*', $tabletvc, "contentid = '$docid' AND tmplvarid = '$tv_id'");

        if ($modx->db->getRecordCount($query) > 0) {
            $row = $modx->db->getRow($query);
            $modx->db->update(['value' => $parent], $tabletvc, "id = '" . $row['id'] . "'");
        } else {
            $modx->db->insert(['contentid' => $docid, 'tmplvarid' => $tv_id, 'value' => $parent], $tabletvc);
        }
    }
}
