﻿/**
 * live_area
 *
 * Жилая площадь
 *
 * @category        tv
 * @name            live_area
 * @internal        @caption Жилая площадь, м&sup2;
 * @internal        @input_type number
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @modx_category Параметры
 * @internal        @rank 5
 * @internal        @template_assignments product
 */