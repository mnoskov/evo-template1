﻿/**
 * kitchen_area
 *
 * Площадь кухни
 *
 * @category        tv
 * @name            kitchen_area
 * @internal        @caption Площадь кухни, м&sup2;
 * @internal        @input_type number
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @modx_category Параметры
 * @internal        @rank 10
 * @internal        @template_assignments product
 */