﻿/**
 * show_on_main
 *
 * Показывать на главной
 *
 * @category        tv
 * @name            show_on_main
 * @internal        @caption Показывать на главной
 * @internal        @input_type checkbox
 * @internal        @input_options Да==1
 * @internal        @input_default 0
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments category,products-list,service,services-list
 */