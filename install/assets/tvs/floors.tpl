﻿/**
 * floors
 *
 * Количество этажей
 *
 * @category        tv
 * @name            floors
 * @internal        @caption Количество этажей
 * @internal        @input_type number
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @modx_category Параметры
 * @internal        @rank 15
 * @internal        @template_assignments product
 */