﻿/**
 * labels
 *
 * Отметки
 *
 * @category        tv
 * @name            labels
 * @internal        @caption Отметки
 * @internal        @input_type checkbox
 * @internal        @input_options Мы рекомендуем==1||Новинка==2||Акция==3||Хит продаж==4
 * @internal        @input_default 0
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments product
 */