﻿/**
 * image
 *
 * Изображение
 *
 * @category        tv
 * @name            image
 * @internal        @caption Изображение
 * @internal        @input_type image
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments service,services-list,product,category,products-list,info,gallery
 */