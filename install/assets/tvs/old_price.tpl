﻿/**
 * old_price
 *
 * Старая цена
 *
 * @category        tv
 * @name            old_price
 * @internal        @caption Старая цена
 * @internal        @input_type number
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments product
 */