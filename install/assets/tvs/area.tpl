﻿/**
 * area
 *
 * Площадь
 *
 * @category        tv
 * @name            area
 * @internal        @caption Площадь, м&sup2;
 * @internal        @input_type number
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @modx_category Параметры
 * @internal        @rank 1
 * @internal        @template_assignments product
 */