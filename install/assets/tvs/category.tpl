﻿/**
 * category
 *
 * Категория
 *
 * @category        tv
 * @name            category
 * @internal        @caption Категория
 * @internal        @description заполняется автоматически
 * @internal        @input_type dropdown
 * @internal        @input_options @EVAL return $modx->runSnippet('getAllCategories');
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @modx_category Параметры
 * @internal        @template_assignments product
 */