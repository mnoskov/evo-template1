/**
 * renderTV
 * 
 * renderTV
 * 
 * @category    snippet
 * @internal    @overwrite true
*/

if (!isset($docid)) {
	$docid = $modx->documentIdentifier;
}
	
$tv = $modx->getTemplateVar($tv, '*', $docid);

if (isset($tv['elements'])) {
	if (!function_exists('ParseIntputOptions')) {
		require_once(MODX_MANAGER_PATH . 'includes/tmplvars.inc.php');
	}
	
	$elements = ParseIntputOptions($tv['elements']);
	
	if (!empty($elements)) {
		foreach ($elements as $element) {
			list($val, $key) = is_array($element) ? $element : explode('==', $element);

			if (strlen($val) == 0) {
				$val = $key;
			}

			if (strlen($key) == 0) {
				$key = $val;
			}

			if ($key == $tv['value']) {
				return $val;
			}
		}
	}
}

return $tv['value'];