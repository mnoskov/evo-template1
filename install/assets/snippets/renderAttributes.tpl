/**
 * renderAttributes
 * 
 * renderAttributes
 * 
 * @category    snippet
 * @internal    @overwrite true
*/

$docid    = isset($docid) ? $docid : $modx->documentIdentifier;
$tpl      = isset($tpl) ? $modx->getChunk($tpl) : '<tr><td>[+caption+]:&nbsp;</td><td>[+value+]</td></tr>';
$outerTpl = isset($outerTpl) ? $modx->getChunk($outerTpl) : '<table>[+wrap+]</table>';
$exclude  = isset($exclude) ? explode(',', $exclude) : [];

if (!isset($params['category'])) {
	return 'Required parameter "category" is missing!';
}

$query = $modx->db->query("
	SELECT tv.*, IFNULL(tvc.value, tv.default_text) AS value
	FROM " . $modx->getFullTableName('site_tmplvars') . " tv
	LEFT JOIN " . $modx->getFullTableName('site_tmplvar_contentvalues') . " tvc ON tvc.tmplvarid = tv.id
	WHERE tv.category = '$category'
	AND tvc.contentid = '$docid'
	AND tv.name NOT IN ('" . implode("', '", $exclude) . "')
	ORDER BY rank;
");

$out = '';

while ($row = $modx->db->getRow($query)) {
	if (!empty($row['elements'])) {
		if (!function_exists('ParseIntputOptions')) {
			require_once(MODX_MANAGER_PATH . 'includes/tmplvars.inc.php');
		}

		if (!function_exists('ProcessTVCommand')) {
			require_once(MODX_MANAGER_PATH . 'includes/tmplvars.commands.inc.php');
		}
		
		$elements = ParseIntputOptions(ProcessTVCommand($row['elements'], '', '', 'tvform', $tv = []));
		$values   = [];

		if (!empty($elements)) {
			foreach ($elements as $element) {
				list($val, $key) = is_array($element) ? $element : explode('==', $element);

				if (strlen($val) == 0) {
					$val = $key;
				}

				if (strlen($key) == 0) {
					$key = $val;
				}

				$values[$key] = $val;
			}
			
			if (isset($values[ $row['value'] ])) {
				$row['value'] = $values[ $row['value'] ];
			}
		}
	}
	
	if (!empty($row['value'])) {
		$out .= $modx->parseText($tpl, $row);
	}
}

if (!empty($out)) {
	$out = $modx->parseText($outerTpl, ['wrap' => $out]);
}

return $out;
