/**
 * categoriesPrepare
 * 
 * categoriesPrepare
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
<?php
if ($_DocLister->getCFGDef('tplCode') == 'categories_tpl3') {
    $tpl_id = $_extDocLister->getStore('category_tpl_id');
    
    if (is_null($tpl_id)) {
        $tpl_id = $modx->db->getValue($modx->db->select('id', $modx->getFullTableName('site_templates'), "templatename = 'category'"));
        $_extDocLister->setStore('category_tpl_id', $tpl_id);
    }
    
    $data['children'] = $modx->runSnippet('DocLister', [
        'parents'      => $data['id'],
        'tpl'          => '@CODE: <a href="[+url+]">[+pagetitle+]</a>',
        'ownerTPL'     => '@CODE: <div class="children">[+dl.wrap+]</div>',
        'addWhereList' => "c.template = '$tpl_id'",
        'orderBy'      => 'c.menuindex ASC',
    ]);
}

return $data;
