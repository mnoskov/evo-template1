/**
 * createFilterConfig
 * 
 * createFilterConfig
 * 
 * @category    snippet
 * @internal    @overwrite true
*/

$fields = @json_decode($fields, true);

if ($fields !== null) {
	$query = $modx->db->query('SELECT tv.caption, tv.id FROM ' . $modx->getFullTablename('site_tmplvars') . ' tv JOIN ' . $modx->getFullTablename('categories') . ' c ON tv.category = c.id WHERE c.category = \'Параметры\'');
	
	$result = $tvs = [];
	
	while ($row = $modx->db->getRow($query)) {
		$tvs[$row['id']] = $row['caption'];
	}
	
	foreach ($fields as $field) {
		if (!isset($tvs[$field[0]])) {
			continue;
		}
		
		$result[] = [
			'param_id'     => $field[0],
			'cat_name'     => '',
			'list_yes'     => 1,
			'fltr_yes'     => 1,
			'fltr_type'    => $field[1],
			'fltr_name'    => $tvs[$field[0]],
			'fltr_many'    => $field[2],
			'param_choose' => '',
		];
	}
	
	$result = [
		'fieldValue'    => $result,
		'fieldSettings' => ['autoincrement' => 1],
	];
	
	$result = str_replace(['{{', '[[', '}}', ']]'], ['{ {', '[ [', '} }', '] ]'], json_encode($result, JSON_UNESCAPED_UNICODE));
	return $result;
}