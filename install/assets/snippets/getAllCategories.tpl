/**
 * getAllCategories
 * 
 * getAllCategories
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
$template_id = $modx->db->getValue($modx->db->select('id', $modx->getFullTableName('site_templates'), "templatename = 'category'"));

$json = $modx->runSnippet('DocLister', [
    'parents'      => '11',
    'showParent'   => '1',
    'api'          => 'pagetitle,id',
    'depth'        => '5',
    'addWhereList' => "c.template = '$template_id'",
]);

$raw = json_decode($json);
$out = [];

foreach ($raw as $row) {
    $out[] = trim($row->pagetitle) . '==' . $row->id;
}

return implode('||', $out);
