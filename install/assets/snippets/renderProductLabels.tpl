/**
 * renderProductLabels
 * 
 * renderProductLabels
 * 
 * @category    snippet
 * @internal    @overwrite true
*/

if (!isset($docid)) {
	$docid = $modx->documentIdentifier;
}
	
$tv = $modx->getTemplateVar('labels', '*', $docid);
$output = '';
$value = explode('||', $tv['value']);

if (isset($tv['elements'])) {
	if (!function_exists('ParseIntputOptions')) {
		require_once(MODX_MANAGER_PATH . 'includes/tmplvars.inc.php');
	}
	
	$elements = ParseIntputOptions($tv['elements']);
	
	if (!empty($elements)) {
		foreach ($elements as $element) {
			list($title, $key) = explode('==', $element);

			if (in_array($key, $value)) {
				$output .= '<span class="label label' . $key . '">' . $title . '</span>';
			}
		}
	}
}

if (!empty($output)) {
	$output = '<span class="labels">' . $output . '</span>';
}

return $output;
