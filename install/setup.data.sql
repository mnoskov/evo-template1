REPLACE INTO `{PREFIX}site_content` (`id`, `pagetitle`, `alias`, `published`, `parent`, `isfolder`, `introtext`, `template`, `menuindex`, `createdby`, `editedby`, `publishedby`, `hidemenu`) VALUES
(10, 'О нас', 'about', 1, 1, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info'), 1, 1, 1, 1, 0),
(11, 'Каталог', 'catalog', 1, 1, 1, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list'), 2, 1, 1, 1, 0),
(15, 'Категория 1', 'kategoriya-1', 1, 11, 1, 'В категории №1 вы найдете самые полезные товары на свете', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category'), 0, 1, 1, 1, 0),
(16, 'Категория 2', 'kategoriya-2', 1, 11, 1, 'В категории №2 вы найдете самые полезные товары на свете', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category'), 1, 1, 1, 1, 0),
(17, 'Категория 3', 'kategoriya-3', 1, 11, 1, 'В категории №3 вы найдете самые полезные товары на свете', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category'), 2, 1, 1, 1, 0),
(18, 'Товар 1', 'tovar-1', 1, 15, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product'), 0, 1, 1, 1, 0),
(19, 'Товар 2', 'tovar-2', 1, 15, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product'), 1, 1, 1, 1, 0),
(20, 'Товар 3', 'tovar-3', 1, 16, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product'), 0, 1, 1, 1, 0),
(21, 'Товар 4', 'tovar-4', 1, 17, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product'), 0, 1, 1, 1, 0),
(12, 'Услуги', 'services', 1, 1, 1, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'), 3, 1, 1, 1, 0),
(22, 'Услуга 1', 'usluga-1', 1, 12, 0, 'Краткое описание услуги', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service'), 0, 1, 1, 1, 0),
(23, 'Услуга 2', 'usluga-2', 1, 12, 0, 'Краткое описание услуги', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service'), 1, 1, 1, 1, 0),
(24, 'Услуга 3', 'usluga-3', 1, 12, 0, 'Краткое описание услуги', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service'), 2, 1, 1, 1, 0),
(13, 'Контакты', 'contacts', 1, 1, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'contacts'), 4, 1, 1, 1, 0),
(14, 'Результаты поиска', 'search', 1, 0, 0, '', (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search'), 3, 1, 1, 1, 1);


REPLACE INTO `{PREFIX}pagebuilder` (`id`, `document_id`, `container`, `title`, `config`, `values`, `visible`, `index`) VALUES
(1, 2, 'main', '', 'main_features', '{\"items\":[{\"title\":\"Проектирование\",\"image\":\"assets/images/demo/example2.jpg\",\"text\":\"Разрабатываем современные проекты жилых домов, коттеджей и бань\"},{\"title\":\"Производство\",\"image\":\"assets/images/demo/example3.jpg\",\"text\":\"Собственное производство газобетонных блоков и железобетонных колец\"},{\"title\":\"Строительство\",\"image\":\"assets/images/demo/example4.jpg\",\"text\":\"Профессиональное строительство загородных домов, коттеджей и бань, в соответствии с ГОСТ\"}]}', 1, 0),
(2, 2, 'main', '', 'main_categories', '{}', 1, 3),
(3, 2, 'main', '', 'main_best', '{\"title\":\"Хиты продаж\",\"btn\":\"Все проекты\"}', 1, 2),
(4, 2, 'main', '', 'main_filter', CONCAT('{\"parameters\":[{\"id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), '\",\"type\":\"1\",\"multiple\":{\"0\":\"1\"}},{\"id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), '\",\"type\":\"6\",\"multiple\":{}},{\"id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), '\",\"type\":\"6\",\"multiple\":{}}]}'), 1, 1),
(5, 2, 'main', '', 'main_numbers', '{\"title\":\"О компании\",\"image\":\"assets/images/demo/example2.jpg\",\"content\":\"<p>Компания \\\"Бумбурум\\\" с 2013 года занимается проектированием и строительством современных загородных домов, коттеджей, бань \\\"под КЛЮЧ\\\"&nbsp;в Перми и Пермском крае&nbsp;с применением материалов: газобетонный блок, кирпич, керамический блок, профилированный брус и бревно.</p>\",\"numbers\":[{\"number\":\"94\",\"text\":\"Построенных объектов\"},{\"number\":\"108\",\"text\":\"Разработанных проектов\"},{\"number\":\"5\",\"text\":\"Лет надежной работы\"}]}', 1, 4),
(6, 2, 'main_cycle', '', 'main_cycle_slide', '{\"title\":\"Строим как для себя!\",\"image\":\"assets/images/demo/example2.jpg\",\"text\":\"Применение только качественных материалов\\nсоответствующих ГОСТу,\\nа так же строгая система контроля на каждом этапе\",\"text_decoration\":\"shadow\",\"link\":\"#\"}', 1, 0),
(7, 2, 'main_cycle', '', 'main_cycle_slide', '{\"title\":\"Собственное производство!\",\"image\":\"assets/images/demo/example3.jpg\",\"text\":\"Один из крупнейших заводов в Перми\\nпо производству газобетонных блоков\\nи железобетонных колец\",\"text_decoration\":\"glowing\",\"link\":\"#\"}', 1, 1);


INSERT INTO `{PREFIX}site_tmplvar_contentvalues` (`tmplvarid`, `contentid`, `value`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 22, 'assets/images/demo/example3.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'tovarparams'), 11, CONCAT('{\"fieldValue\":[{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Цена, руб.\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Площадь, м²\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'live_area'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Жилая площадь, м²\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'kitchen_area'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Площадь кухни, м²\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'bedrooms'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"1\",\"fltr_name\":\"Количество спален\",\"fltr_many\":\"1\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'floors'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"1\",\"fltr_name\":\"Количество этажей\",\"fltr_many\":\"1\",\"param_choose\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'tovarparams'), 14, CONCAT('{\"fieldValue\":[{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"1\",\"fltr_name\":\"Категория\",\"fltr_many\":\"1\",\"fltr_href\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Цена, руб.\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Площадь, м²\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'live_area'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Жилая площадь, м²\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'kitchen_area'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"6\",\"fltr_name\":\"Площадь кухни, м²\",\"fltr_many\":\"\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'bedrooms'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"1\",\"fltr_name\":\"Количество спален\",\"fltr_many\":\"1\",\"param_choose\":\"\"},{\"param_id\":\"', (SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'floors'), '\",\"cat_name\":\"\",\"list_yes\":\"\",\"fltr_yes\":\"1\",\"fltr_type\":\"1\",\"fltr_name\":\"Количество этажей\",\"fltr_many\":\"1\",\"param_choose\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'show_on_main'), 17, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 17, 'assets/images/demo/example4.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 17, '4'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'show_on_main'), 16, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 16, 'assets/images/demo/example2.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 16, '4'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'show_on_main'), 15, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 15, 'assets/images/demo/example1.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 15, '4'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'live_area'), 21, '50'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'bedrooms'), 21, '2'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'floors'), 21, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'kitchen_area'), 21, '12'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), 21, '75.3'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 21, '17'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'prints'), 21, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example7.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example8.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example9.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), 21, '5000'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'old_price'), 21, '5500'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'gallery'), 21, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example2.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example3.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example4.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example5.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example6.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'best'), 21, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 21, 'assets/images/demo/example1.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'prints'), 20, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example7.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example8.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example9.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), 20, '5100'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 20, 'assets/images/demo/example1.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'gallery'), 20, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example2.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example3.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example4.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example5.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example7.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example8.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'bedrooms'), 20, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'best'), 20, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 20, '16'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'live_area'), 20, '40'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'floors'), 20, '2'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'kitchen_area'), 20, '11'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), 20, '63'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'best'), 19, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'bedrooms'), 19, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'live_area'), 19, '40'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'floors'), 19, '2'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'kitchen_area'), 19, '12'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), 19, '77'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 19, '15'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'prints'), 19, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example7.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example8.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example9.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), 19, '5200'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 19, 'assets/images/demo/example1.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'gallery'), 19, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example2.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example3.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example4.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example5.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example7.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example8.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'category'), 18, '15'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'bedrooms'), 18, '2'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'live_area'), 18, '50'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'floors'), 18, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'kitchen_area'), 18, '12'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'area'), 18, '47.3'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'price'), 18, '5300'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'old_price'), 18, '5500'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'image'), 18, 'assets/images/demo/example1.jpg'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'best'), 18, '1'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'prints'), 18, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example7.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example8.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example9.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}'),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'gallery'), 18, '{\"fieldValue\":[{\"thumb\":\"\",\"image\":\"assets/images/demo/example2.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example3.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example4.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example5.jpg\",\"title\":\"\"},{\"thumb\":\"\",\"image\":\"assets/images/demo/example6.jpg\",\"title\":\"\"}],\"fieldSettings\":{\"autoincrement\":1}}');


# прикрепляем tv к шаблонам

DELETE FROM `{PREFIX}site_tmplvar_templates` WHERE tmplvarid in (SELECT id FROM {PREFIX}site_tmplvars WHERE name IN('tovarparams', 'meta_description', 'meta_keywords', 'meta_title', 'og_description', 'og_image', 'og_title'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'tovarparams'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'tovarparams'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'tovarparams'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'tovarparams'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_keywords'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'meta_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_description'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_image'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'));

INSERT INTO `{PREFIX}site_tmplvar_templates` (`tmplvarid`, `templateid`) VALUES
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'category')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'gallery')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'info')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'main')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'product')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'products-list')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'search')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'service')),
((SELECT id FROM {PREFIX}site_tmplvars WHERE name = 'og_title'), (SELECT id FROM {PREFIX}site_templates WHERE templatename = 'services-list'));


