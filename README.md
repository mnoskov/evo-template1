Порядок установки:
* SimpleGallery
* MultiTV
* PageBuilder
* ClientSettings
* eFilter

Модули из приватного репозитория:

* Customizer
* DLInstagram
* FaviconCreator
* LessCompiler
* blank
* затем сам шаблон

После установки настроить модуль eLists, настроить плагин SimpleGallery, заполнить клиентские настройки.

<img src="https://monosnap.com/file/rvfms9gfgltyRbBjK84b4Kt0kHv5IK.png">

<img src="https://monosnap.com/file/xXC7jC4DEtCGVfNAXrrDKrXQl6nVBY.png">
