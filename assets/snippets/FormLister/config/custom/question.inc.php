<?php

if (!defined('IN_PARSER_MODE')) {
    return [];
}

return [
    'rules' => [
        'msg' => [
            'required' => 'Не заполнено сообщение!',
        ],
        '!name' => [
            'lengthBetween' => [
                'params'  => [2, 100],
                'message' => 'Имя должно быть от 2 до 100 символов',
            ]
        ],
        'phone' => [
            'required' => 'Введите номер телефона',
            'matches' => [
                'params'  => '/^\+?[78]\s?\(\d{3}\)\s?\d{3}-\d\d-\d\d$/',
                'message' => 'Формат телефона неверный',
            ]
        ],
    ],
    'subject' => 'Вопрос с сайта!',
    'reportTpl' => '@CODE:
        <b>Вопрос с сайта!</b>
        <table>
            <tr><td>Имя:&nbsp;</td><td>[+name.value+]</td></tr>
            <tr><td>Телефон:&nbsp;</td><td>[+phone.value+]</td></tr>
            <tr><td>Сообщение:&nbsp;</td><td>[+msg.value+]</td></tr>
            <tr><td>Страница:&nbsp;</td><td>[+page.value+]</td></tr>
        </table>
    ',
];
