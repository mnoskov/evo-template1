<?php

    return [
        'title' => 'Описание и числа',

        'show_in_templates' => 1,

        'container' => 'main',

        'templates' => [
            'owner' => '
                <div class="about section">
                    <div class="container">
                        <h1 class="block-title">
                            [+title+]
                        </h1>

                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <img src="[[phpthumb? &input=`[+image+]` &options=`w=240,h=240,zc=1`]]" class="img-fluid">
                            </div>
                            
                            <div class="col-sm-8 col-md-9">
                                <div class="user-content">
                                    [+content+]
                                </div>
                                
                                <div class="props row">
                                    [+numbers+]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ',

            'numbers' => '
                <div class="col-md-4 col-xs-6">
                    <div class="value animate" data-value="[+number+]">0</div>
                    <div class="text">[+text+]</div>
                </div>
            ',
        ],

        'fields' => [
            'title' => [
                'caption' => 'Заголовок',
                'type'    => 'text',
            ],

            'image' => [
                'caption' => 'Изображение',
                'type'    => 'image',
            ],

            'content' => [
                'caption' => 'Текст',
                'type'    => 'richtext',
                'theme'   => 'mini',
            ],

            'numbers' => [
                'caption' => 'Числа',
                'type'    => 'group',
                'layout'  => 'horizontal',
                'fields'  => [
                    'number' => [
                        'caption' => 'Число',
                        'type'    => 'text',
                    ],

                    'text' => [
                        'caption' => 'Текст',
                        'type'    => 'text',
                    ],
                ],
            ],
        ],
    ];

