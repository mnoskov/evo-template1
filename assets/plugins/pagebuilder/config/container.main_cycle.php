<?php

    return [
        'title' => 'Слайдер',

        'show_in_templates' => 1,

        'placement' => 'tab',

        'templates' => [
            'owner' => '
                <div class="main-cycle">
                    <div class="slick" data-slick=\'{"dots": true, "arrows": true, "autoplay": true}\'>
                        [+wrap+]
                    </div>
                </div>
            ',
        ],
    ];

