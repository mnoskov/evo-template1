<?php

return [
    'title' => 'Лента Instagram',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="instagram">
                <div class="container">
                    <div class="block-title">
                        [+title+]
                    </div>
                </div>
                
                [[DLInstagram? 
                    &token=`[+token+]`
                    &display=`16`
                    &tpl=`@CODE:<li><a href="[+link+]" target="_blank" rel="nofollow"><span style="background-image: url(\'[+images.standard_resolution.url+]\');"></span></a>`
                    &ownerTPL=`@CODE:<ul class="items">[+dl.wrap+]</ul>`
                ]]
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Мы в Instagram',
        ],

        'login' => [
            'caption' => 'Логин в Instagram',
            'type' => 'text',
        ],

        'token' => [
            'caption' => 'Токен',
            'type' => 'text',
            'note' => 'Можно получить по ссылке - <a href="http://instagram.pixelunion.net/">http://instagram.pixelunion.net/</a>',
        ],
    ],
];

