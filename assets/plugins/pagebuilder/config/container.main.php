<?php

    return [
        'title' => 'Состав главной страницы',

        'show_in_templates' => 1,

        'placement' => 'content',

        'templates' => [
            'owner' => '[+wrap+]',
        ],
    ];

