<?php

    return [
        'title' => 'Категории',

        'show_in_templates' => 1,

        'container' => 'main',

        'templates' => [
            'owner' => '
                <div class="section">
                    <div class="container">
                        <div class="block-title">[+title+]</div>

                        [[DocLister? 
                            &parents=`[+source+]`
                            &tvList=`image`
                            &tpl=`[+template+]_item`
                            &ownerTPL=`[+template+]_wrap`
                            &orderBy=`c.menuindex ASC`
                            &filters=`tv:show_on_main:=:1`
                            &prepare=`categoriesPrepare`
                            &tplCode=`[+template+]`
                        ]]
                    </div>
                </div>
            ',
        ],

        'fields' => [
            'title' => [
                'caption' => 'Заголовок блока',
                'type' => 'text',
                'default' => 'Категории',
            ],

            'source' => [
                'caption' => 'Источник данных',
                'type' => 'dropdown',
                'elements' => '@SELECT pagetitle, id FROM ' . $this->modx->getFullTablename('site_content') . ' WHERE parent = 1 AND deleted = 0',
                'default' => 11,
            ],

            'template' => [
                'caption' => 'Шаблон вывода категорий',
                'type' => 'imageradio',
                'layout' => 'horizontal',
                'elements' => [
                    'categories_tpl1' => '/assets/plugins/pagebuilder/images/categories_tpl1.png',
                    'categories_tpl2' => '/assets/plugins/pagebuilder/images/categories_tpl2.png',
                    'categories_tpl3' => '/assets/plugins/pagebuilder/images/categories_tpl3.png',
                ],
                'default' => 'categories_tpl1',
            ],
        ],
    ];

