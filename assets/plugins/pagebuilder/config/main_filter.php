<?php

    return [
        'title' => 'Фильтр товаров',

        'show_in_templates' => 1,

        'container' => 'main',

        'templates' => [
            'owner' => '
                [!eFilter? &cfg=`template1_main` &docid=`11` &autoSubmit=`1` &ajax=`1` &removeDisabled=`1` &tvConfig=`[[createFilterConfig? &fields=`[+parameters+]`]]`!]
                [+eFilter_form+]
            ',
        ],

        'prepare' => function(&$options, &$values) {
            $values['parameters'] = array_map(function($row) {
                return [$row['id'], $row['type'], !empty($row['multiple'][0]) ? 1 : 0];
            }, $values['parameters']);

            $options['fields']['parameters'] = ['type' => 'text'];
            $values['parameters'] = str_replace(['{{', '[[', '}}', ']]'], ['{ {', '[ [', '} }', '] ]'], json_encode($values['parameters']));
        },

        'fields' => [
            'parameters' => [
                'type' => 'group',
                'layout' => 'horizontal',
                'caption' => 'Параметры в фильтре',
                'fields' => [
                    'id' => [
                        'type' => 'dropdown',
                        'elements' => '@SELECT tv.caption, tv.id FROM ' . $this->modx->getFullTablename('site_tmplvars') . ' tv JOIN ' . $this->modx->getFullTablename('categories') . ' c ON tv.category = c.id WHERE c.category = \'Параметры\'',
                    ],

                    'type' => [
                        'type' => 'dropdown',
                        'elements' => [1 => 'Чекбокс', 'Список', 'Диапазон', 'Флажок', 'Мультиселект', 'Слайдер', 'Цвет', 'Паттерн'],
                    ],

                    'multiple' => [
                        'type' => 'checkbox',
                        'elements' => [1 => 'Множественный'],
                    ],
                ],
            ],
        ],
    ];

