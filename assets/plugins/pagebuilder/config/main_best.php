<?php

    return [
        'title' => 'Хиты продаж',

        'show_in_templates' => 1,

        'container' => 'main',

        'templates' => [
            'owner' => '
                <div class="section gray">
                    <div class="container">
                        <a href="[~11~]" class="btn btn-hollow-theme pull-xs-right">[+btn+]</a>
                        <div class="block-title">[+title+]</div>

                        [[DocLister? 
                            &parents=`11`
                            &depth=`3` 
                            &tvList=`image,area,price,old_price`
                            &orderBy=`price ASC`
                            &tpl=`products_item` 
                            &ownerTPL=`products_cycle`
                            &addWhereList=`c.template = ' . $this->modx->db->getValue($this->modx->db->select('id', $this->modx->getFullTableName('site_templates'), "templatename = 'product'")) . '`
                            &filters=`tv:best:=:1`
                        ]]
                    </div>
                </div>
            ',
        ],

        'fields' => [
            'title' => [
                'caption' => 'Заголовок',
                'type'    => 'text',
            ],

            'btn' => [
                'caption' => 'Текст кнопки',
                'type'    => 'text',
            ],
        ],
    ];

