<?php

return [
    'title' => 'Партнеры',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="partners section gray">
                <div class="container">
                    <div class="slick" data-slick=\'{"arrows": true, "autoplay": true, "slidesToShow": 5, "responsive": [{"breakpoint": 991, "settings": {"slidesToShow": 4}}, {"breakpoint": 767, "settings": {"slidesToShow": 3}}, {"breakpoint": 575, "settings": {"slidesToShow": 2}}]}\'>
                        [+items+]
                    </div>
                </div>
            </div>
        ',

        'items' => '
            <span class="slide">
                <img src="[[phpthumb? &input=`[+image+]` &options=`w=150,h=100`]]" alt="[+title+]">
            </span>
        ',
    ],

    'fields' => [
        'items' => [
            'caption' => 'Логотипы',
            'type'    => 'group',
            'layout'  => 'gallery',
            'fields'  => [
                'title' => [
                    'caption' => 'Название',
                    'type'    => 'text',
                ],

                'image' => [
                    'caption' => 'Логотип',
                    'type'    => 'image',
                ],
            ],
        ],
    ],
];

