<?php

    return [
        'title' => 'Слайд',

        'show_in_templates' => 1,

        'container' => 'main_cycle',

        'templates' => [
            'owner' => '
                <div class="slide" style="background-image: url(\'[+image+]\');">
                    <div class="container text-xs-center text-lg-left">
                        <div class="title">
                            <span class="inline-padding"><span><span>[[nl2br? &in=`[+title+]`]]</span></span></span>
                        </div>

                        <div class="text">
                            [[if? &is=`[+text_decoration+]:is:bg` &then=`
                                <span class="inline-padding"><span><span>[[nl2br? &in=`[+text+]`]]</span></span></span>
                            ` &else=`
                                <span class="[+text_decoration+]">[[nl2br? &in=`[+text+]`]]</span>
                            `]]
                            
                        </div>
                        
                        <a href="[+link+]" class="btn btn-theme">Перейти</a>
                    </div>
                </div>
            ',
        ],

        'fields' => [
            'title' => [
                'caption' => 'Заголовок',
                'type'    => 'textarea',
                'height'  => '60px',
            ],

            'image' => [
                'caption' => 'Изображение',
                'type'    => 'image',
            ],

            'text' => [
                'caption' => 'Текст',
                'type'    => 'textarea',
                'height'  => '80px',
            ],

            'text_decoration' => [
                'caption'  => 'Оформление текста',
                'type'     => 'radio',
                'elements' => ['' => 'Нет', 'bg' => 'Подложка', 'glowing' => 'Свечение', 'shadow' => 'Тень'],
                'layout'   => 'horizontal',
            ],

            'link' => [
                'caption' => 'Ссылка',
                'type'    => 'text',
            ],
        ],
    ];

