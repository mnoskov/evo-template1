$(function() {
    $(document).on('click', '#main-submit', function(e) {
        this.form.action = this.getAttribute('data-url');
        $(document).off('submit', 'form#eFiltr');
    });

    $('[data-gallery="fancybox"]').fancybox();

    $('.filter').each(function() {
        var $range = $(this).find('.value-range');

        if ($range.length) {
            $range.nstSlider({
                crossable_handles: false,
                left_grip_selector: '.grip.left',
                right_grip_selector: '.grip.right',
                value_bar_selector: '.bar',
                value_changed_callback: function(cause, leftValue, rightValue) {
                    var $inputs = $(this).parent().find('input');
                    $inputs.eq(0).val(leftValue);
                    $inputs.eq(1).val(rightValue);
                }
            });

            $(window).resize(function() {
                $range.nstSlider('refresh');
            });
        }
    });

    $('.head-menu')
        .on('click', '.show-menu', function(e) {
            e.preventDefault();
            $('.head-menu').toggleClass('show');
        })
        .on('click', '.category-menu-item > a', function(e) {
            var $self = $(this),
                $item = $self.parent(),
                $menu = $self.closest('.head-menu');

            if ($menu.hasClass('show') && !$item.hasClass('show')) {
                e.preventDefault();
                $menu.find('.show').removeClass('show');
                $item.addClass('show');
            }
        })
        .on('click', '.icon-search', function(e) {
            e.preventDefault();
            $(this).closest('.menu').next('.search-wrap').toggleClass('show');
        });

    $('.head').on('click', '.show-menu', function(e) {
        e.preventDefault();

        $(this).closest('.head').next('.head-menu').addClass('show-fixed').find('.menu')
            .css('margin-left', $(this).position().left + 15)
            .one('mouseleave', function() {
                $(this).closest('.head-menu').removeClass('show-fixed');
            })
            .children('.main').css('min-width', $(this).outerWidth());
    });

    (function($head) {
        $(window)
            .on('scroll resize', function() {
                var fixed = $head.hasClass('fixed');

                if ($(this).scrollTop() > 170) {
                    if (!fixed) {
                        $head.addClass('fixed');
                    }
                } else {
                    if (fixed) {
                        $head.removeClass('fixed');
                        $head.children('.head-menu').removeClass('show-fixed').find('.menu').removeAttr('style');
                    }
                }
            })
            .load(function() {
                $(this).scroll();
            });
    })($('.head-placeholder'));

    $('.props').runOnScroll(function($block) {
        var $items = $block.find('.value.animate'),
            max = 1;

        $items.each(function() {
            max = Math.max(max, $(this).attr('data-value'));
        });

        max = 1 / max;

        $items.each(function() {
            var val = $(this).attr('data-value');
            $(this).animateNumber({number: val}, 2000 + val * max * 1000);
        });
    });
});
