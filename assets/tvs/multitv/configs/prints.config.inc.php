<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
	'thumb' => array(
		'caption' => 'Thumbnail',
		'type' => 'thumb',
		'thumbof' => 'image'
	),
	'image' => array(
		'caption' => 'Изображение',
		'type' => 'image'
	),
	'title' => array(
		'caption' => 'Подпись',
		'type' => 'text'
	),
);
$settings['templates'] = array(
	'outerTpl' => '
		<hr>
		<div class="prints">
			<div class="block-title">Планировки</div>
			<ul class="items">
				[+wrapper+]
			</ul>
		</div>',
	'rowTpl' => '<li><a href="[[phpthumb? &input=`[+image+]` &options=`w=1600,h=1600`]]" data-fancybox="prints" data-caption="[+e_title+]"><span class="image"><img src="[[phpthumb? &input=`[+image+]` &options=`w=260,h=260`]]" alt="[+title:htmlspecialchars+]"></span></a>'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false,
);

$settings['prepare'] = function($data, $modx, $_mTV) {
	$data['e_title'] = htmlspecialchars($data['title']);
	return $data;
};
