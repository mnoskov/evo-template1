<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
	'thumb' => array(
		'caption' => 'Thumbnail',
		'type' => 'thumb',
		'thumbof' => 'image'
	),
	'image' => array(
		'caption' => 'Изображение',
		'type' => 'image'
	),
	'title' => array(
		'caption' => 'Подпись',
		'type' => 'text'
	),
);
$settings['templates'] = array(
	'outerTpl' => '
		<div class="images slick" data-slick=\'{ "asNavFor": ".thumbs" }\'>
			[+wrapper+]
		</div>',
	'rowTpl' => '<div class="slide">
		<a href="[[thumb? &input=`[+image+]` &options=`w=1600,h=1600,f=jpg`]]" data-fancybox="images" data-caption="[+e_title+]" style="background-image: url([[thumb? &input=`[+image+]` &options=`w=635,h=350,f=jpg`]]);"></a>
	</div>'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false,
);

$settings['prepare'] = function($data, $modx, $_mTV) {
	$data['e_title'] = htmlspecialchars($data['title']);
	return $data;
};
