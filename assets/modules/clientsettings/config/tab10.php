<?php

return [
    'caption' => 'Информация о компании',
    'settings' => [
        'company_logo' => [
            'caption' => 'Логотип',
            'type' => 'image',
            'note' => 'Если не указан, будет использован логотип по умолчанию',
        ],
        'favicon' => [
            'caption' => 'Фавикон',
            'type' => 'image',
        ],
        'company_address' => [
            'caption' => 'Адрес компании',
            'type'  => 'text',
            'default_text' => 'г.Пермь, ул.Уличная, д.37, офис 142'
        ],
        'company_phone' => [
            'caption' => 'Контактный телефон компании',
            'type'  => 'text',
            'default_text' => '8 800 123 4567',
        ],
        'company_email' => [
            'caption' => 'Контактный email компании',
            'type'  => 'text',
            'default_text' => 'mail@' . $_SERVER['HTTP_HOST'],
        ],
        'head_text' => [
            'caption' => 'Текст в шапке',
            'type'  => 'textareamini',
            'default_text' => 'Проектирование и строительство современных домов в Перми и крае',
        ],
        'policy' => [
            'caption' => 'Политика конфиденциальности',
            'type' => 'file',
        ],
        'images_alg' => [
            'caption' => 'Обработка изображений',
            'type' => 'dropdown',
            'elements' => 'Обрезать==0||Вписать (с белыми краями)==1',
            'default_value' => 0,
        ],
    ],
];
