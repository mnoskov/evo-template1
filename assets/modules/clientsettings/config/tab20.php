<?php

return [
    'caption'    => 'Социальные сети',
    'subcaption' => 'Если какая-либо ссылка не заполнена - она не будет показываться. Также отображение должно быть предусмотрено в шаблоне.',
    'settings' => [
        'social_facebook' => [
            'caption' => 'Facebook',
            'type'  => 'text',
            'default_text' => '#',
        ],
        'social_vkontakte' => [
            'caption' => 'ВКонтакте',
            'type'  => 'text',
            'default_text' => '#',
        ],
        'social_instagram' => [
            'caption' => 'Instagram',
            'type'  => 'text',
            'default_text' => '#',
        ],
        'social_youtube' => [
            'caption' => 'Youtube',
            'type'  => 'text',
            'default_text' => '#',
        ],
        'social_odnoklassniki' => [
            'caption' => 'Одноклассники',
            'type'  => 'text',
            'default_text' => '#',
        ],
    ],
];
